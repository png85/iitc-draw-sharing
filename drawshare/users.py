from flask import (Blueprint, request, redirect, abort,
                   url_for, session, flash, current_app, render_template)
from drawshare.db import get_db
from drawshare.auth import login_required, roles_required
import click

bp = Blueprint('users', __name__, url_prefix='/users')


@bp.route('/<username>')
@login_required
def profile(username):
    db = get_db()
    user_data = db.execute("""SELECT users.id AS user_id, users.username,
    users.v_enlid, users.v_verified, users.v_points, users.v_level,
    (SELECT COUNT(id) FROM user_draws WHERE user_id=users.id) AS draw_count_total,
    (SELECT COUNT(id) FROM user_draws WHERE user_id=users.id AND share_mode='public') AS draw_count_public
    FROM users
    WHERE username=?""",
                           (username,)).fetchone()

    if not user_data:
        abort(404)

    return render_template('users/profile.html', user_data=user_data)


@bp.route('/admin/list')
@roles_required(['Site Admin', 'User Manager'])
def admin_list():
    db = get_db()
    users = db.execute("""SELECT users.id AS user_id, users.username, users.banned,
    users.v_enlid, users.v_verified, users.v_points, users.v_level,
    (SELECT COUNT(id) FROM user_draws WHERE user_id=users.id) AS draw_count,
    (SELECT COUNT(role_id) FROM user_roles WHERE user_id=users.id) AS role_count,
    (password_hash IS NOT NULL) AS has_password
    FROM users
    ORDER BY users.username""").fetchall()

    return render_template('users/admin/list.html', users=users)


@bp.route('/admin/<id>/edit', methods=('POST', 'GET'))
@roles_required(['Site Admin', 'User Manager'])
def admin_edit(id):
    db = get_db()

    # Fetch base user data
    user_data = db.execute(
        """SELECT users.id, users.username FROM users WHERE id=?""", (id,)).fetchone()
    if not user_data:
        flash('Invalid user ID selected.', 'danger')
        return redirect(url_for('users.admin_list'))

    # Build list of role IDs for all roles the user currently has
    user_roles = []
    user_roles_result = db.execute(
        """SELECT role_id FROM user_roles WHERE user_id=?""", (id,)).fetchall()
    for role in user_roles_result:
        user_roles.append(role['role_id'])

    # Fetch data for all grantable roles
    all_roles = db.execute("""SELECT roles.id, roles.role_name, roles.role_description
    FROM roles WHERE roles.is_grantable=TRUE
    ORDER BY roles.role_name""").fetchall()

    if request.method == 'POST':
        if 'roles[]' in request.form:
            selected_roles = list(
                map(lambda x: int(x), request.form.getlist('roles[]')))

            # Build list of roles to insert
            roles_to_insert = []
            for role_id in selected_roles:
                if not role_id in user_roles:
                    roles_to_insert.append(role_id)

            # Build list of roles to delete
            roles_to_delete = []
            for role_id in user_roles:
                if not role_id in selected_roles:
                    roles_to_delete.append(role_id)

            # Update user_roles table
            roles_changed = False
            for role_id in roles_to_insert:
                db.execute(
                    '''INSERT INTO user_roles (user_id, role_id) VALUES (?, ?)''', (id, role_id,))
                roles_changed = True

            for role_id in roles_to_delete:
                db.execute(
                    '''DELETE FROM user_roles WHERE user_id=? AND role_id=?''', (id, role_id,))
                roles_changes = True

            if roles_changed:
                flash('User profile for {} updated.'.format(
                    user_data['username']), 'success')
                db.commit()

        else:
            # No roles were, selected, make sure to delete all user_roles records
            db.execute('DELETE FROM user_roles WHERE user_id=?', (id,))
            db.commit()

        return redirect(url_for('users.admin_list'))

    return render_template('users/admin/edit.html', user_data=user_data, user_roles=user_roles, roles=all_roles)


@bp.route('/admin/<id>/delete', methods=['POST'])
@roles_required(['Site Admin', 'User Manager'])
def admin_delete(id):
    db = get_db()
    try:
        delete_shares_query = "DELETE FROM user_draw_shares WHERE shared_by=? OR shared_with=?"
        db.execute(delete_shares_query, (id, id,))

        delete_draws_query = "DELETE FROM user_draws WHERE user_id=?"
        db.execute(delete_draws_query, (id,))

        delete_roles_query = "DELETE FROM user_roles WHERE user_id=?"
        db.execute(delete_roles_query, (id,))

        delete_user_query = "DELETE FROM users WHERE id=?"
        db.execute(delete_user_query, (id,))

        db.commit()
        flash('Deleted user with ID {}'.format(id), 'success')

    except db.Error as ex:
        db.rollback()
        flash('Failed to delete user: {}'.format(ex.message), 'danger')

    return redirect(url_for('users.admin_list'))


@bp.route('/admin/<id>/ban', methods=['POST'])
@roles_required(['Site Admin', 'User Manager'])
def admin_ban(id):
    db = get_db()
    user_data = db.execute(
        '''SELECT username, banned FROM users WHERE id=?''', (id,)).fetchone()
    if not user_data:
        return redirect(url_for('users.admin_list'))

    toggle_query = ''
    if user_data['banned']:
        toggle_query = 'UPDATE users SET banned=FALSE WHERE id=?'
    else:
        toggle_query = 'UPDATE users SET banned=TRUE WHERE id=?'

    current_app.logger.debug(toggle_query)
    db.execute(toggle_query, (id,))
    db.commit()
    flash('Ban flag toggled for {}'.format(user_data['username']), 'success')

    return redirect(url_for('users.admin_list'))


@bp.cli.command('make-admin')
@click.argument('username')
def cli_make_admin(username):
    """Add administrative roles to given username."""
    db = get_db()
    user_data = db.execute(
        '''SELECT users.id FROM users WHERE username=?''', (username,)).fetchone()

    if not user_data:
        click.echo('User "{}" not found!'.format(username))
    else:
        # Load any roles the user might already have
        existing_roles = []
        existing_roles_result = db.execute('''SELECT user_roles.role_id, roles.role_name
        FROM user_roles
        INNER JOIN roles ON user_roles.role_id=roles.id
        WHERE user_roles.user_id=?''', (user_data['id'],)).fetchall()
        for existing_role in existing_roles_result:
            existing_roles.append(existing_role['id'])

        # Fetch IDs of roles the user doesn"t have yet
        roles_to_add = []
        for role_name in ['Site Admin', 'User Manager']:
            role_result = db.execute(
                '''SELECT id FROM roles WHERE role_name=?''', (role_name,)).fetchone()
            if role_result['id'] not in existing_roles:
                roles_to_add.append(role_result['id'])

        roles_added = 0
        for role in roles_to_add:
            db.execute(
                '''INSERT INTO user_roles (user_id, role_id) VALUES (?, ?)''', (user_data['id'], role))
            roles_added += 1

        if roles_added > 0:
            db.commit()
            click.echo('Added roles to user.')
