-- Users allowed to access the site
DROP TABLE IF EXISTS users;
CREATE TABLE users (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    username TEXT UNIQUE NOT NULL,
    password_hash TEXT DEFAULT NULL,
    allow_password_login INTEGER NOT NULL DEFAULT 0,
    banned INTEGER NOT NULL DEFAULT 0,
    v_verified INTEGER NOT NULL DEFAULT 0,
    v_enlid TEXT DEFAULT NULL,
    v_points INTEGER NOT NULL DEFAULT 0,
    v_level INTEGER NOT NULL DEFAULT 0
);

-- Roles that can be inherited by users
DROP TABLE IF EXISTS roles;
CREATE TABLE roles (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    role_name TEXT UNIQUE NOT NULL,
    role_description TEXT,
    is_grantable INTEGER NOT NULL DEFAULT 1
);

INSERT INTO roles (role_name, role_description) VALUES ('Site Admin', 'Access to admin menus');
INSERT INTO roles (role_name, role_description) VALUES ('User Manager', 'Manage users and permissions');

-- User/Role connections
DROP TABLE IF EXISTS user_roles;
CREATE TABLE user_roles (
    user_id INTEGER NOT NULL,
    role_id INTEGER NOT NULL,

    FOREIGN KEY(user_id) REFERENCES users(id),
    FOREIGN KEY(role_id) REFERENCES roles(id)
);
CREATE UNIQUE INDEX idx_user_roles_unique ON user_roles(user_id, role_id);

-- Draws uploaded by users
DROP TABLE IF EXISTS user_draws;
CREATE TABLE user_draws(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    user_id INTEGER NOT NULL,
    added TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    share_mode TEXT NOT NULL DEFAULT 'public',
    title TEXT NOT NULL,
    json TEXT NOT NULL,

    FOREIGN KEY(user_id) REFERENCES users(id),
    CHECK(share_mode IN ('public', 'unlisted', 'explicit'))
);

-- Draw shares from users to users
DROP TABLE IF EXISTS user_draw_shares;
CREATE TABLE user_draw_shares(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    draw_id INTEGER NOT NULL,
    shared_by INTEGER NOT NULL,
    shared_with INTEGER NOT NULL,
    can_edit INTEGER NOT NULL DEFAULT 0,

    FOREIGN KEY(draw_id) REFERENCES user_draws(id),
    FOREIGN KEY(shared_by) REFERENCES users(id),
    FOREIGN KEY(shared_with) REFERENCES users(id)
);
CREATE UNIQUE INDEX idx_user_draw_shares ON user_draw_shares(draw_id, shared_by, shared_with);
