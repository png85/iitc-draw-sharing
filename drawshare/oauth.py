from flask import (Blueprint, request, redirect,
                   url_for, session, flash, current_app)

from drawshare.db import get_db
import voauth

bp = Blueprint('oauth', __name__, url_prefix='/oauth')


@bp.route('/v')
def v_response():
    # Make sure we have an OAuth state hash in our session
    if not 'v_oauth_state' in session:
        flash('Missing V OAuth state in session.', 'danger')
        return redirect(url_for('auth.login'))

    # Make sure all required parameters are present
    for k in ['code', 'state']:
        if not k in request.args:
            flash('Missing parameter: {}'.format(k), 'danger')
            return redirect(url_for('auth.login'))

    # Make sure state returned by V matches the one in session
    if request.args['state'] != session['v_oauth_state']:
        flash('Invalid V OAuth state in session', 'danger')
        redirect(url_for('auth.login'))

    authenticator = voauth.Authenticator()
    authenticator.set_scopes([voauth.SCOPE_PROFILE])
    authenticator.client_id = current_app.config['V_API_CLIENT_ID']
    authenticator.client_secret = current_app.config['V_API_SECRET']
    authenticator.code = request.args['code']
    authenticator.set_redirect_url('{}{}'.format(request.host_url.rstrip('/'),
                                                 url_for('oauth.v_response')))

    access_token = authenticator.get_token(request.args['state'])
    if not access_token:
        flash('Failed to obtain V API access token!', 'danger')
        return redirect(url_for('auth.login'))

    v_info = authenticator.get_v_info()

    # Check V profile against configured flags
    if 'V_PROFILE_REQUIRED_FLAGS' in current_app.config:
        required_flags = current_app.config['V_PROFILE_REQUIRED_FLAGS']
        reject_reasons = []
        for key in required_flags:
            (expected, message) = required_flags[key]

            # Make sure V info contains requested flag
            if not key in v_info:
                reject_reasons.append(
                    'Missing flag "{}" in V info.'.format(key))
                continue

            # Check V flag against expected value
            if v_info[key] != expected:
                reject_reasons.append(message)

        if reject_reasons:
            for reason in reject_reasons:
                flash('Access denied: {}'.format(reason), 'danger')
            return redirect(url_for('auth.login'))

    db = get_db()
    agent_name = v_info['agent']
    user_id = None

    #
    # Check if an user with the given enlid exists
    user_by_enlid = db.execute(
        'SELECT id FROM users WHERE v_enlid=?', (v_info['enlid'],)).fetchone()
    if user_by_enlid:
        user_id = user_by_enlid['id']

    #
    # Check if an user record with the returned agent name and no enlid exists
    if not user_id:
        unlinked_user = db.execute(
            'SELECT id FROM users WHERE username=? AND v_enlid IS NULL', (agent_name,)).fetchone()
        if unlinked_user:
            user_id = unlinked_user['id']

            flash('Linked enlid "{}" to user "{}"'.format(
                v_info['enlid'], agent_name), info)
            db.execute('UPDATE users SET v_enlid=? WHERE id=?',
                       (v_info['enlid'], user_id,))
            db.commit()

    if not user_id:
        # No user reacord found, create a new one unless registration is currently disabled
        if 'REGISTRATION_ALLOWED' in current_app.config and current_app.config['REGISTRATION_ALLOWED'] is True:
            insert_result = db.execute(
                'INSERT INTO users (username, v_enlid) VALUES (?, ?)', (agent_name, v_info['enlid'],))
            user_id = insert_result.lastrowid
            db.commit()

        else:
            flash('Creating new accounts is currently disabled.', 'danger')
            return redirect(url_for('auth.login'))

    # Update V-related info in existing user record
    db.execute('UPDATE users SET v_points=?, v_level=?, v_verified=?, username=? WHERE id=?', (v_info['vpoints'],
                                                                                               v_info['vlevel'],
                                                                                               v_info['verified'],
                                                                                               agent_name,
                                                                                               user_id,))
    db.commit()

    session.clear()
    session['user_id'] = user_id

    flash('You have been logged in using V.', 'success')
    return redirect(request.url_root)
