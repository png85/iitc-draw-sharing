import sqlite3
import click
from flask import current_app, g
from flask.cli import with_appcontext


def get_db():
    """Get handle to database connection for current request."""
    if 'db' not in g:
        g.db = sqlite3.connect(current_app.config['DATABASE'],
                               detect_types=sqlite3.PARSE_DECLTYPES)
        g.db.row_factory = sqlite3.Row

    return g.db


def close_db(e=None):
    """Close current request's database connection

    This is registered to be called when the app context is being torn down.
    """
    db = g.pop('db', None)
    if db is not None:
        db.close()


def init_db():
    """Implementation of 'init-db'' command to re-create database."""
    db = get_db()
    with current_app.open_resource('schema.sql') as script:
        db.executescript(script.read().decode('utf8'))


@click.command('init-db')
@with_appcontext
def init_db_command():
    """Clear existing database and recreate new tables."""
    init_db()
    click.echo('Database initialized successfully.')


def init_app(app):
    """Register teardown callback with app instance."""
    app.teardown_appcontext(close_db)
    app.cli.add_command(init_db_command)
