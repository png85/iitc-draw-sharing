from flask import (Blueprint, g, redirect, render_template,
                   request, url_for, flash, abort, current_app)
from werkzeug.exceptions import abort
import functools
from drawshare.auth import login_required
from drawshare.db import get_db

bp = Blueprint('draws', __name__, url_prefix='/draws')


@bp.route('/')
@login_required
def index():
    db = get_db()

    my_draws = db.execute('''SELECT user_draws.id, user_draws.user_id AS author_id, ? AS author_name,
    user_draws.added, user_draws.title,
    TRUE AS can_edit 
    FROM user_draws WHERE user_draws.user_id=?''', (g.current_user['username'], g.current_user['id'],)).fetchall()

    public_draws_query = """SELECT
    user_draws.id, user_draws.user_id AS author_id, users.username AS author_name, user_draws.added, user_draws.title
    FROM user_draws
    INNER JOIN users ON user_draws.user_id = users.id
    WHERE (user_draws.user_id!=? AND share_mode='public')
    ORDER BY user_draws.added DESC"""
    public_draws = db.execute(
        public_draws_query, (g.current_user['id'],)).fetchall()

    shared_draws_query = """SELECT user_draw_shares.draw_id AS id,
    user_draws.user_id AS author_id, users.username AS author_name, user_draws.added, user_draws.title,
    user_draw_shares.can_edit
    FROM user_draw_shares
    INNER JOIN user_draws ON user_draw_shares.draw_id = user_draws.id
    INNER JOIN users ON user_draws.user_id = users.id
    WHERE user_draw_shares.shared_with=?
    """
    shared_draws = db.execute(
        shared_draws_query, (g.current_user['id'],)).fetchall()

    #
    # Build list of all available draws
    draw_ids = []
    draws = []
    for draw in my_draws:
        draws.append(dict(zip(draw.keys(), draw)))
        draw_ids.append(draw['id'])

    for draw in shared_draws:
        if not draw['id'] in draw_ids:
            draws.append(dict(zip(draw.keys(), draw)))
            draw_ids.append(draw['id'])

    for draw in public_draws:
        if not draw['id'] in draw_ids:
            draws.append(dict(zip(draw.keys(), draw)))
            draw_ids.append(draw['id'])

    #
    # Sort combined draw list by creation date
    draws.sort(key=lambda x: x['added'], reverse=True)

    return render_template('draws/index.html', draws=draws)


def view_permission_required(view):
    """Check if user is allowed to view a given draw."""
    @functools.wraps(view)
    def wrapped_view(**kwargs):
        if not 'id' in kwargs:
            abort(500, 'Missing parameter')

        draw_id = kwargs['id']
        db = get_db()
        draw = db.execute(
            'SELECT * FROM user_draws WHERE id=?', (draw_id,)).fetchone()

        if not draw:
            abort(404, 'Draw not found')

        # Explicit sharing mode needs further lookup, public and unlisted can always be viewed
        if draw['share_mode'] == 'explicit':
            if draw['user_id'] != g.current_user['id']:
                share = db.execute('SELECT * FROM user_draw_shares WHERE draw_id=? AND shared_with=?',
                                   (draw_id, g.current_user['id'],)).fetchone()
                if not share:
                    # Draw wasn't shared with current user
                    abort(403, 'Draw not shared with {}'.format(
                        g.current_user['username']))

        return view(**kwargs)

    return wrapped_view


def edit_permission_required(view):
    """Check if user is allowed to edit requested draw."""
    @functools.wraps(view)
    def wrapped_view(**kwargs):
        if not 'id' in kwargs:
            abort(500, 'Missing parameter')

        draw_id = kwargs['id']
        db = get_db()
        draw = db.execute('SELECT * FROM user_draws WHERE id=?',
                          (draw_id, )).fetchone()

        if not draw:
            abort(404, 'Draw not found')

        # Look up permissions from explicit shares in case user doesn't own the draw
        if draw['user_id'] != g.current_user['id']:
            share = db.execute('SELECT * FROM user_draw_shares WHERE draw_id=? AND shared_with=?',
                               (draw_id, g.current_user['id'],)).fetchone()
            if not share:
                # Draw wasn't shared with current user
                abort(403, 'Draw not shared with {}'.format(
                    g.current_user['username']))

            if not share['can_edit']:
                # Draw was shared in read-only mode
                abort(
                    403, 'Draw is read-only for {}'.format(g.current_user['username']))

        return view(**kwargs)

    return wrapped_view


@bp.route('/create', methods=('GET', 'POST'))
@login_required
def create():
    if request.method == 'POST':
        form_ok = True
        # Make sure all required parameters are present
        for (param, descr) in [('title', 'Draw Title'),
                               ('json', 'Exported IITC JSON text'),
                               ('share_mode', 'Sharing Mode')]:
            if not param in request.form:
                flash('Missing required form value: {}'.format(descr), 'danger')
                form_ok = False

        # Make sure the selected sharing mode is valid
        if 'share_mode' in request.form and not request.form['share_mode'] in ['public', 'unlisted', 'explicit']:
            flash('Invalid sharing mode selected.', 'alert')
            form_ok = False

        if form_ok:
            db = get_db()
            query = 'INSERT INTO user_draws (user_id, title, json, share_mode) VALUES (?, ?, ?, ?)'
            db.execute(query,
                       (g.current_user['id'],
                        request.form['title'],
                        request.form['json'],
                        request.form['share_mode'],))
            db.commit()

            return redirect(url_for('draws.index'))

    return render_template('draws/create.html')


@bp.route('/<id>/delete', methods=['POST'])
@login_required
@edit_permission_required
def delete(id):
    """Delete a stored draw."""
    db = get_db()

    # Check if given draw ID is valid
    draw = db.execute('SELECT * FROM user_draws WHERE id=?', (id,)).fetchone()
    if draw is None:
        flash('Draw not found.', 'danger')
        return redirect(url_for('draws.index'))

    db.execute('DELETE FROM user_draws WHERE id=?', (id,))
    db.commit()

    return redirect(url_for('draws.index'))


@bp.route('/<id>/edit', methods=('GET', 'POST'))
@login_required
@edit_permission_required
def edit(id):
    """Edit an existing draw."""
    db = get_db()
    draw_data = db.execute(
        'SELECT * FROM user_draws WHERE id=?', (id,)).fetchone()

    if request.method == 'POST':
        form_ok = True
        for (key, descr) in [('title', 'Draw Title'),
                             ('json', 'DrawTools JSON Data'),
                             ('share_mode', 'Sharing Mode')]:
            if not key in request.form:
                flash('Missing required form field: {}'.format(descr), 'danger')
                form_ok = False

        if form_ok and not request.form['share_mode'] in ['public', 'unlisted', 'explicit']:
            flash('Invalid sharing mode selected.', 'danger')
            form_ok = False

        if not form_ok:
            return redirect(url_for('draws.edit', id=id))

        record_updated = False
        for key in ['title', 'json', 'share_mode']:
            if request.form[key] != draw_data[key]:
                query = "UPDATE user_draws SET {}=? WHERE id=?".format(key)
                db.execute(query, (request.form[key], id,))
                record_updated = True

        if record_updated:
            db.commit()
            flash('Draw updated successfully.', 'success')
            redirect(url_for('draws.edit', id=id))

    shares_query = """SELECT user_draw_shares.id AS share_id,
    user_draw_shares.shared_with AS user_id,
    users.username AS username,
    user_draw_shares.can_edit
    FROM user_draw_shares
    INNER JOIN users ON user_draw_shares.shared_with = users.id
    WHERE user_draw_shares.draw_id=?
    ORDER BY users.username"""
    shares = db.execute(shares_query, (id,)).fetchall()

    return render_template('draws/edit.html', draw=draw_data, shares=shares)


@bp.route('/<id>/add_share_search')
@login_required
@edit_permission_required
def add_share_search(id):
    """AJAX Endpoint to search for users when sharing draws."""
    if not 'q' in request.args:
        return ''

    db = get_db()

    blacklisted_user_ids = [g.current_user['id']]
    existing_shares = db.execute('SELECT shared_with AS user_id FROM user_draw_shares WHERE user_draw_shares.draw_id=?',
                                 (id,)).fetchall()
    if existing_shares:
        for share in existing_shares:
            blacklisted_user_ids.append(share['user_id'])

    users = db.execute('SELECT username, id FROM USERS WHERE username LIKE ?',
                       ('{}%'.format(request.args['q']),)).fetchall()
    if not users:
        return ''

    return render_template('draws/add_share_search.html', users=users, blacklisted_user_ids=blacklisted_user_ids, draw_id=id)


@bp.route('/<id>/share/add')
@login_required
@edit_permission_required
def add_share(id):
    """Share draw with another user."""
    redirect_url = url_for('draws.edit', id=id)
    for k in ['with', 'mode']:
        if not k in request.args:
            flash('Missing arguments.', 'danger')
            return redirect(redirect_url)

        if not request.args['mode'] in ['read', 'write']:
            flash('Invalid access mode.', 'danger')
            return redirect(redirect_url)

    current_app.logger.debug('Found all required parameters.')

    # Sharing with ourselves is pointless since we can already edit
    if request.args['with'] == g.current_user['id']:
        flash('Sharing draws with yourself is pointless.', 'warning')
        return redirect(redirect_url)

    current_app.logger.debug('Target user differs from current user.')

    db = get_db()
    existing_share = db.execute('SELECT * FROM user_draw_shares WHERE draw_id=? AND shared_with=?',
                                (id, request.args['with'])).fetchone()

    can_edit = (request.args['mode'] == 'write')
    record_updated = False
    if not existing_share:
        # No share was found yet, create new record
        db.execute('INSERT INTO user_draw_shares (draw_id, shared_with, shared_by, can_edit) VALUES (?, ?, ?, ?)',
                   (id, request.args['with'], g.current_user['id'], can_edit))
        record_updated = True
    else:
        # Document is already shared with user, update access and shared_by flags
        db.execute('UPDATE user_draw_shares SET shared_by=?, can_edit=? WHERE id=?',
                   (g.current_user['id'], can_edit, existing_share['id']))
        record_updated = True

    if record_updated:
        flash('Draw shared successfully.', 'success')
        db.commit()

    return redirect(redirect_url)


@bp.route('/<id>/unshare/<share_id>')
@login_required
@edit_permission_required
def delete_share(id, share_id):
    """Remove existing share record for a draw."""
    db = get_db()
    db.execute(
        'DELETE FROM user_draw_shares WHERE id=? AND draw_id=?', (share_id, id,))
    db.commit()

    return redirect(url_for('draws.edit', id=id))


@bp.route('/<id>/view')
@login_required
@view_permission_required
def view(id):
    """Display a stored draw."""
    db = get_db()
    draw_data = db.execute(
        """SELECT user_draws.id, user_draws.user_id AS author_id, user_draws.title, user_draws.added, user_draws.json,
        users.username AS author_name
        FROM user_draws
        INNER JOIN users ON users.id = user_draws.user_id
        WHERE user_draws.id=?""", (id,)).fetchone()
    if not draw_data:
        return redirect(url_for('draws.index'))

    can_edit = False
    if draw_data['author_id'] == g.current_user['id']:
        can_edit = True
    else:
        share = db.execute('SELECT * FROM user_draw_shares WHERE draw_id=? AND shared_with=?',
                           (id, g.current_user['id'],)).fetchone()
        if not share:
            can_edit = False
        else:
            can_edit = share['can_edit']

    return render_template('draws/view.html', draw=draw_data, can_edit=can_edit)
