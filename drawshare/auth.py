import functools
from hashlib import md5
from flask import (Blueprint, flash, g, redirect, abort,
                   render_template, request, session, url_for, current_app)
import werkzeug.security as wzs
from drawshare.db import get_db

import voauth
import random
import string

bp = Blueprint('auth', __name__, url_prefix='/auth')


@bp.route('/register', methods=('GET', 'POST'))
def register():
    if 'REGISTRATION_ALLOWED' in current_app.config and current_app.config['REGISTRATION_ALLOWED'] is False:
        flash('Registration of new accounts is currently disabled. Please try again later.', 'danger')
        return redirect(url_for('auth.login'))

    if 'AUTHENTICATION_METHODS' in current_app.config and not 'password' in current_app.config['AUTHENTICATION_METHODS']:
        flash('Password-based registration is disabled. Please try to use another method.', 'danger')
        return redirect(url_for('auth.login'))

    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        db = get_db()
        error = None

        if not username:
            error = 'Username is required'
        elif not password:
            error = 'Password is required'
        elif db.execute('SELECT id FROM users WHERE username = ?', (username,)).fetchone() is not None:
            error = 'User {} is already registered.'.format(username)

        if error is None:
            password_hash = wzs.generate_password_hash(password)
            db.execute(
                'INSERT INTO users (username, password_hash, allow_password_login) VALUES (?, ?, 1)',
                (username, password_hash,))
            db.commit()

            return redirect(url_for('auth.login'))

        flash(error, 'danger')

    return render_template('auth/register.html')


@bp.route('/login', methods=('GET', 'POST'))
def login():
    if request.method == 'POST':
        if 'AUTHENTICATION_METHODS' in current_app.config and not 'password' in current_app.config['AUTHENTICATION_METHODS']:
            flash('Password-based login is disabled. Please try to use another method.', 'danger')
            return redirect(url_for('auth.login'))
            
        username = request.form['username']
        password = request.form['password']
        db = get_db()
        error = None
        user = db.execute(
            'SELECT * FROM users WHERE username = ?', (username,)
        ).fetchone()

        if user is None:
            error = 'Incorrect username.'
        elif not user['allow_password_login']:
            error = 'This account does not allow password-based login.'
        elif not wzs.check_password_hash(user['password_hash'], password):
            error = 'Incorrect password.'

        if user['banned']:
            error = 'Your account has been banned.'

        if error is None:
            session.clear()
            session['user_id'] = user['id']
            return redirect(request.url_root)

        flash(error, 'danger')

    v_authenticator = voauth.Authenticator()
    v_authenticator.set_redirect_url('{}{}'.format(request.host_url.rstrip('/'),
                                                   url_for('oauth.v_response')))
    v_authenticator.set_scopes([voauth.SCOPE_PROFILE])
    v_authenticator.client_id = current_app.config['V_API_CLIENT_ID']
    v_authenticator.client_secret = current_app.config['V_API_SECRET']
    state = str(''.join(random.choice(string.ascii_letters))
                for _ in range(16)).encode('utf-8')
    v_authenticator.state = state
    session['v_oauth_state'] = md5(state).hexdigest()

    v_auth_url = v_authenticator.get_auth_url()

    return render_template('auth/login.html', v_auth_url=v_auth_url, config=current_app.config)


@bp.before_app_request
def load_logged_in_user():
    try:
        user_id = session['user_id']

        if user_id is None:
            g.current_user = None
        else:
            db = get_db()

            #
            # Store dictionary containing user data in g
            user_row = db.execute(
                'SELECT * FROM users WHERE id=?', (user_id,)).fetchone()
            g.current_user = dict(zip(user_row.keys(), user_row))

            #
            # Make sure the user isn"t banned
            if user_row['banned']:
                abort(403, 'You have been banned!')

            #
            # Fetch user roles and store them in the g.current_user dictionary
            #
            # Roles are stored in a dictionary of the form
            #   { 'role-name': { <database columns for role-name> }}
            # to allow easy presence checks by role name.
            roles_rows = db.execute("""SELECT
            user_roles.role_id,
            roles.role_name, roles.role_description
            FROM user_roles
            INNER JOIN roles ON user_roles.role_id=roles.id
            WHERE user_roles.user_id=?
            ORDER BY roles.role_name""", (user_id,)).fetchall()

            g.current_user['roles'] = {}
            for role in roles_rows:
                g.current_user['roles'][role['role_name']] = dict(
                    zip(role.keys(), role))

    except KeyError:
        g.current_user = None


@bp.route('/logout')
def logout():
    session.clear()
    return redirect(request.url_root)


def login_required(view):
    @functools.wraps(view)
    def wrapped_view(**kwargs):
        if g.current_user is None:
            flash('You have been logged out.', 'info')
            return redirect(url_for('auth.login'))

        return view(**kwargs)

    return wrapped_view


def roles_required(role_names=[]):
    """Check if user has given roles.

    Checks if the user is logged in and has all of the requested roles.

    Args:
        role_names (list, optional): Names of all roles required to access the decorated view
    """
    def wrapper(view):
        @functools.wraps(view)
        def wrapped_view(*args, **kwargs):
            # Make sure the user is logged in
            if g.current_user is None:
                flash(
                    'Access denied. You need to be logged in to access this page.', 'danger')
                return redirect(url_for('auth.login'))

            # Allow access if no roles are required
            if not role_names:
                return view(*args, **kwargs)

            # Make sure current user has all specified roles
            for role in role_names:
                if not role in g.current_user['roles']:
                    flash('Access denied. You need to have the "{}" role to access this page.'.format(
                        role), 'danger')
                    return redirect(request.url_root)

            return view(*args, **kwargs)

        return wrapped_view

    return wrapper
